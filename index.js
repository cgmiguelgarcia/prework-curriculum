var iconid="";

function cambiarfoto(e){                            //función para sustituir la foto por la foto2

  var nombrefoto=document.getElementById(e).style.backgroundImage;
  var nombrefoto2="";
  for(i=0;i<nombrefoto.length-6;i++){
    nombrefoto2=nombrefoto2+nombrefoto[i];
  }
  return(nombrefoto2+'2.jpg")');
}

function restituirfoto(e){                            //función para sustituir la foto por la foto2

  var nombrefoto=document.getElementById(e).style.backgroundImage;
  var nombrefoto2="";
  for(i=0;i<nombrefoto.length-7;i++){
    nombrefoto2=nombrefoto2+nombrefoto[i];
  }
  return(nombrefoto2+'.jpg")');
}


function desplegarmenu(e,e2) {

  var resultoptions=document.getElementsByName("contentoption");
  var menuoptions=document.getElementsByName("menuoptions");
  var desencadenante=document.getElementById(e2);

  for(var i=0;i<resultoptions.length;i++){
    resultoptions[i].classList.replace("active","inactive");
    menuoptions[i].classList.remove(menuoptions[i].classList.value);
    menuoptions[i].classList.add(e);
  }
  
  document.getElementById(e).classList.replace("inactive","active");
  desencadenante.classList.remove(desencadenante.classList.value);
  desencadenante.classList.add("menuselected");
  reseteardesplegados(true,true,true)

};

function mousemenu(e,bigicon){

  document.getElementById(e).style.textShadow="0px 0px 2px rgba(226, 188, 63, 0.7), 0px 0px 4px rgba(226, 188, 63, 0.8), 1px 1px 8px rgba(226, 188, 63, 0.9), 0px 0px 16px rgb(226, 188, 63)";
  
  if(document.getElementById(e).style.backgroundImage!=""){
    document.getElementById(e).style.setProperty("background-image",""+cambiarfoto(e));
    document.getElementById(e).classList.remove(document.getElementById(e).classList);
    document.getElementById(e).classList.add("bigicon");
    document.getElementById(bigicon).classList.remove(document.getElementById(bigicon).classList);
    document.getElementById(bigicon).classList.add("bigiconanother");
  }else if(e[0]+e[1]+e[2]+e[3]=="icon"){
    document.getElementById(e).classList.replace("icon","bigicon");
  }

};
  
function mousemenuout(e,bigicon){

  document.getElementById(e).style.textShadow="none";

  if(document.getElementById(e).style.backgroundImage!="" && iconid!=e){
    document.getElementById(e).style.setProperty("background-image",""+restituirfoto(e));
    document.getElementById(e).classList.remove(document.getElementById(e).classList);
    document.getElementById(e).classList.add("icon");
    document.getElementById(bigicon).classList.remove(document.getElementById(bigicon).classList);
    document.getElementById(bigicon).classList.add("inactive");
  }else if(e[0]+e[1]+e[2]+e[3]=="icon" && iconid!=e){
    document.getElementById(e).classList.replace("bigicon","icon");    
  }

};

function reseteardesplegados(iconselected,bigicon,iconanother){                         //función para añadir a todos los click que hacen reset en las pestañas

  if(iconselected=true){

    for(i=0;i<9;i++){
      document.getElementById("experience"+(1+i)).classList.remove("iconselected");
      document.getElementById("experience"+(1+i)).classList.add("inactive");
    }

    for(i=0;i<5;i++){
      document.getElementById("aptitudes"+(1+i)).classList.remove("iconaptitudselected");
      document.getElementById("aptitudes"+(1+i)).classList.add("inactive");
    }

  }

  if(bigicon=true){

    iconid="";

    for(i=0;i<9;i++){
      document.getElementById("iconexperience"+(1+i)).classList.remove("bigicon");
      document.getElementById("iconexperience"+(1+i)).classList.add("icon");
    }

    for(i=0;i<3;i++){
      document.getElementById("iconstudies"+(1+i)).classList.remove("bigicon");
      document.getElementById("iconstudies"+(1+i)).classList.add("icon");
    }

    for(i=0;i<5;i++){
      document.getElementById("iconaptitudes"+(1+i)).classList.remove("bigicon");
      document.getElementById("iconaptitudes"+(1+i)).classList.add("icon");
    }

  }

  if(iconanother=true){
    var theicon=document.getElementsByName("othericon");
    var thebigicon=document.getElementsByName("otherbigicon");
    var theselecticon=document.getElementsByName("otherselecticon");

    for(var i=0;i<theicon.length;i++){
      theicon[i].classList.remove(theicon[i].classList);
      theicon[i].classList.add("icon");
    }

    for(var i=0;i<thebigicon.length;i++){
      thebigicon[i].classList.remove(thebigicon[i].classList);
      thebigicon[i].classList.add("inactive");
    }

    for(var i=0;i<theselecticon.length;i++){
      theselecticon[i].classList.remove(theselecticon[i].classList);
      theselecticon[i].classList.add("inactive");
    }

  }



  var resetaptitudes1=document.getElementsByName("desplegaraptitudesopen");
  var resetaptitudes2=document.getElementsByName("desplegaraptitudesclose");
  for(var i=0;i<resetaptitudes1.length;i++){                                 //reseteo de los menus desplegados en aptitudes
    resetaptitudes1[i].classList.remove("inactive");
  }
  for(var i=0;i<resetaptitudes2.length;i++){
    resetaptitudes2[i].classList.remove("inactive");
    resetaptitudes2[i].classList.add("inactive");
  }
  state=0;
}

function desplegariconexp(e){                                               //click en icono de experiencia laboral

  reseteardesplegados(true,true,false)

  iconid="icon"+e

  document.getElementById(e).classList.replace("inactive","iconselected");
  document.getElementById(iconid).classList.replace("icon","bigicon");

}

function desplegariconstu(e){

  reseteardesplegados(false,true,false)

  iconid="icon"+e

  document.getElementById(iconid).classList.replace("icon","bigicon");

}

function desplegariconapt(e){

  reseteardesplegados(true,true,false)

  iconid="icon"+e

  document.getElementById(e).classList.replace("inactive","iconaptitudselected");
  document.getElementById(iconid).classList.replace("icon","bigicon");

}

function desplegaraptitudes(e){

  if(document.getElementById(e+"b").classList.contains("inactive")){
    document.getElementById(e+"a").classList.add("inactive");
    document.getElementById(e+"b").classList.remove("inactive");
  }else{
    document.getElementById(e+"a").classList.remove("inactive");
    document.getElementById(e+"b").classList.add("inactive");
  }

}

function desplegariconanother(icon, bigicon, iconselected){

  if(document.getElementById(iconselected).classList!="iconimageselected"){

  reseteardesplegados(false,false,true);
  
  document.getElementById(icon).style.setProperty("background-image",""+restituirfoto(icon));
  document.getElementById(icon).classList.remove(document.getElementById(icon).classList);
  document.getElementById(icon).classList.add("inactive");
  document.getElementById(bigicon).classList.remove(document.getElementById(bigicon).classList);
  document.getElementById(bigicon).classList.add("inactive");
  document.getElementById(iconselected).classList.remove(document.getElementById(iconselected).classList);
  document.getElementById(iconselected).classList.add("iconimageselected");

  iconid=icon;
  }else{reseteardesplegados(false,false,true);}
  
}

function sociallinks(e,e2){

  document.getElementById(e).setAttribute("src",e2);

}

